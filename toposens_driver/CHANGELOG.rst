^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package toposens_driver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2019-05-14)
------------------
* Exec CI on 8 runners
* Debugged driver tests in CI pipeline
* Added setup delay to Hz tests
* Contributors: Adi Singh, Christopher Lang, Sebastian Dengler

0.9.6 (2019-05-13)
------------------
* Add colored tags to tests for console debugging
* Added sensor mesh to PCL rviz
* Removed pointcloud dependency on rviz
* Implemented test data download functionality
* Contributors: Adi Singh, Roua Mokchah

0.9.5 (2019-05-03)
------------------
* Implemented tests for dynamic reconfig
* Implemented tests for toposens_pointcloud
* Contributors: Adi Singh, Roua Mokchah

0.9.3 (2019-04-26)
------------------
* Added automated tests for toposens_markers
* Contributors: Adi Singh, Roua Mokchah

0.9.2 (2019-04-17)
------------------
* Fixed package versions
* Added Docker config file
* Contributors: Adi Singh

0.9.0 (2019-04-09)
------------------
* Integrated with turtlebot packages
* Linted cmake files
* Contributors: Adi Singh, Nancy Seckel

0.8.1 (2019-03-29)
------------------
* Added gitlab CI config
* Added remote Turtlebot launchers
* Pointcloud instructions added to README.md
* Temperature calibration implemented
* Contributors: Adi Singh, Andrej Wallwitz, Nancy Seckel, Sebastian Dengler

0.8.0 (2019-03-08)
------------------
* Added driver documentation
* Created Command class with overloaded constructors
* Contributors: Adi Singh, Christopher Lang

0.7.0 (2019-02-27)
------------------
* Added native PCL integration
* Implemented markers using rviz visual tools
* Added dynamic reconfigure capability for driver
* Refactored to standard ROS package template
* Contributors: Adi Singh

0.5.0 (2019-02-07)
------------------
