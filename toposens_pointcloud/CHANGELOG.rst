^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package toposens_pointcloud
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.0.0 (2019-05-14)
------------------
* Exec CI on 8 runners
* Debugged driver tests in CI pipeline
* Added setup delay to Hz tests
* Contributors: Adi Singh, Christopher Lang, Sebastian Dengler

0.9.6 (2019-05-13)
------------------
* Add colored tags to tests for console debugging
* Added sensor mesh to PCL rviz
* Removed pointcloud dependency on rviz
* Implemented test data download functionality
* Contributors: Adi Singh, Roua Mokchah

0.9.5 (2019-05-03)
------------------
* Implemented tests for toposens_pointcloud
* Contributors: Roua Mokchah

0.9.3 (2019-04-26)
------------------
* Rebased to clean history
* Contributors: Adi Singh

0.9.2 (2019-04-17)
------------------
* Fixed package versions
* Contributors: Adi Singh

0.9.0 (2019-04-09)
------------------

0.8.1 (2019-03-29)
------------------
* Added remote Turtlebot launchers
* Temperature calibration implemented
* Contributors: Nancy Seckel, Sebastian Dengler

0.8.0 (2019-03-08)
------------------
* Created Command class with overloaded constructors
* Contributors: Christopher Lang

0.7.0 (2019-02-27)
------------------
* Added native PCL integration
* Contributors: Adi Singh

0.5.0 (2019-02-07)
------------------
